var express = require('express');
var router = express.Router();
var passport = require('./../../config/passport');
var authApi = require('./../../controllers/api/AuthApi');

router.post('/authenticate', authApi.authenticate);
router.post('/forgotPassword', authApi.forgotPassword);
router.post('/facebook_token', passport.authenticate('facebook-token'), authApi.authFacebookToken);

module.exports = router;