var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var bicycleSchema = new Schema({
    code: Number,
    colour: String,
    model: String,
    location: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
});

bicycleSchema.statics.createInstance = function(code, colour, model, location) {
    return new this({
        code: code,
        colour: colour,
        model: model,
        location: location
    });
};

bicycleSchema.methods.toString = () => {
    return `code: ${this.code} | colour: ${this.colour}`;
};

bicycleSchema.statics.allBicycles = function(cb) {
    return this.find({}, cb);
}

bicycleSchema.statics.add = function(bici, cb) {
    this.create(bici, cb);
}

bicycleSchema.statics.findByCode = function(code, cb) {
    return this.findOne({code, cb}, cb);
}

bicycleSchema.statics.removeByCode = function(code, cb) {
    return this.deleteOne({code, cb}, cb);
}

module.exports = mongoose.model('Bicycle', bicycleSchema);