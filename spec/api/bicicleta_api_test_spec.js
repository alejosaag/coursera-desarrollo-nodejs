var mongoose = require('mongoose');
var request = require('request');
var server = require('./../../bin/www');
var Bicicleta = require('./../../models/Bicicleta');

var baseUrl = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta api', () => {
    beforeEach(function(done) {
        var mongoDb = 'mongodb://localhost:27017/testdb';
        mongoose.disconnect();
        mongoose.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.log.bind(console, 'MongoDb connection error'));
        db.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Bicicleta.deleteMany({}, (err, success) => {
            if (err) console.log(err);

            done();
        });
    });
    
    describe('GET: /bicicletas/', () => {
        it('Status 200', (done) => {

            request.get(baseUrl, function(error, response, body) {
                var result = JSON.parse(body);

                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST: /bicicletas/create', () => {
        it('Status 200', (done) => {
            var headers = {'Content-Type': 'application/json'};

            var a = '{"id": 4, "color": "Negro", "modelo": "Urbana", "lat": 3.446241, "lng": -76.518911}';

            request.post({
                headers,
                url: `${baseUrl}/create`,
                body: a
            }, function(error, response, body) {
                var bici = JSON.parse(body).bicicleta;
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(2).color).toBe('Negro');
                done();
            });
        });
    });

    /*describe('PUT: /bicicletas/update/id', () => {
        it('Status 200', (done) => {
            var bici = new Bicicleta(1, 'Rojo', 'Urbana', [3.444978,-76.517538]);
            Bicicleta.add(bici);

            var headers = {'Content-Type': 'application/json'};

            var a = '{"id": 1, "color": "Negro", "modelo": "Montaña", "lat": 3.446241, "lng": -76.518911}';

            request.put({
                headers,
                url: 'http://localhost:3000/api/bicicletas/update/1',
                body: a
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(1).color).toBe('Negro');
                done();
            });
        });
    });

    describe('DELETE: /bicicletas/delete/id', () => {
        it('Status 204', (done) => {
            var biciA = new Bicicleta(1, 'Rojo', 'Urbana', [3.444978,-76.517538]);
            var biciB = new Bicicleta(2, 'Negro', 'Urbana', [3.446241,-76.518911]);
            
            Bicicleta.add(biciA);
            Bicicleta.add(biciB);

            var headers = {'Content-Type': 'application/json'};

            var a = '{"id": 1}';

            request.delete({
                headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: a
            }, function(error, response, body) {
                expect(response.statusCode).toBe(204);
                expect(Bicicleta.allBicis.length).toBe(1);
                done();
            });
        });
    });*/
});