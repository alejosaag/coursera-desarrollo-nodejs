var mongoose = require('mongoose');
var Bicicleta = require('./../../models/Bicicleta');
var Usuario = require('./../../models/Usuario');
var Reserva = require('./../../models/Reserva');

describe('Testing bicycles', function() {
    beforeEach(function(done) {
        var mongoDb = 'mongodb://localhost:27017/testdb';
        mongoose.disconnect();
        mongoose.connect(mongoDb, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.log.bind(console, 'MongoDb connection error'));
        db.once('open', () => {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach((done) => {
        Reserva.deleteMany({}, (err, success) => {
            if (err) console.log(err);

            Usuario.deleteMany({}, (err, success) => {
                if (err) console.log(err);

                Bicicleta.deleteMany({}, (err, success) => {
                    if (err) console.log(err);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'Rojo', 'Urbana', [3.444978,-76.517538]);

            expect(bici.code).toBe(1);
            expect(bici.colour).toBe("Rojo");
            expect(bici.model).toBe("Urbana");
            expect(bici.location[0]).toEqual(3.444978);
            expect(bici.location[1]).toEqual(-76.517538);
        });
    });

    describe('Bicicleta.allBicyles', () => {
        it('Comienza vacia', (done) => {
            Bicicleta.allBicycles(function(error, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Agrega una bici', (done) => {
            var bici = Bicicleta.createInstance(1, 'Rojo', 'Urbana', [3.444978,-76.517538]);
            Bicicleta.add(bici, function(err, newBici) {
                if (err) console.log(err);

                Bicicleta.allBicycles(function(err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(bici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Consultar bicicleta con code 1', (done) => {
            Bicicleta.allBicycles(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var biciA = new Bicicleta({code: 1, colour: 'Rojo', model: 'Urbana', location: [3.444978,-76.517538]});
                Bicicleta.add(biciA, function(err, newBici) {
                    if (err) console.log(err);

                    var biciB = new Bicicleta({code: 2, colour: 'Negro', model: 'Urbana', location: [3.446241,-76.518911]});
                    Bicicleta.add(biciB, function(err, newBici) {
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(err, bici) {
                            expect(bici.code).toBe(biciA.code);
                            expect(bici.colour).toBe(biciA.colour);
                            expect(bici.model).toBe(biciA.model);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('Debe existor la reserva', (done) => {
            const user = new Usuario({nombre: 'Luna'});
            user.save();
            const bici = new Bicicleta({code: 1, colour: 'Rojo', model: 'Urbana', location: [3.444978,-76.517538]});
            bici.save();

            var today = new Date();
            var tomorrow = new Date();
            tomorrow.setDate(today.getDate() + 1);

            user.reserve(bici._id, today, tomorrow, function(err, reserved) {
                Reserva.find({}).populate('bicycle').populate('user').exec(function(err, reserves) {
                    console.log(reserves[0]);
                    expect(reserves.length).toBe(1);
                    expect(reserves[0].reserveDays()).toBe(2);
                    expect(reserves[0].bicycle.code).toBe(1);
                    expect(reserves[0].user.nombre).toBe(user.nombre);
                    done();
                });
            });
        });
    });
});