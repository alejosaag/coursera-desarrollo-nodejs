# **Bienvenido a EXPRESS**



# Curso: Desarrollo del lado servidor: NodeJS, Express y Mongo

Código de implementación de aplicación web con express en NodeJS.



## Inicio


### Clonar repositorio.

Para clonar el repositorio utiliza el siguiente comando:

```
git clone https://bitbucket.org/alejosaag/coursera-desarrollo-nodejs.git
```

### Instalación de dependencias.

Usar el sigueinte comando para instalar las dependencias requeridas:

```
npm install
```

### Ejecución.

Una vez instaladas las dependencias, para iniciar la ejecución del servidor ejecutar el comando:

```
npm run devStart
```

Si el servidor se ha iniciado sin problemas ingresa en un navegador web la dirección:

```
http://localhost:3000/bicicletas
```

## Estructura

El repositorio esta compuesto por el código del proyecto realizado y la colección de *Postman*